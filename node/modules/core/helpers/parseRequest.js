/**
 * Created by adlab on 21.01.16.
 */
export function parsePcRequest(request) {
    return request.payload.reduceRight((accum, element) => {
        let rowData = element.split(":").map(el => el.trim());
        return {
            ...accum,
            [rowData.shift().toLowerCase()]: rowData.shift()
        }
    }, {
        uri: request.basic.replace('HTTP/1.1', '').replace(/[GET]/g, '').trim()
    });
}

export const shouldSavePcRequest = data => data.accept.indexOf("text/html") !== -1
