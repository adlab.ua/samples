import {CHROME_EXTENSION_TYPE, PC_CLIENT_TYPE} from './../contants'
import {parsePcRequest, shouldSavePcRequest} from './../helpers/parseRequest'
import {Request} from './../documents/Request'

let match = cases => event => cases.hasOwnProperty(event.type) ? cases[event.type](event) : cases.hasOwnProperty('default') ? cases.default(event) : false
let logError = (err) => console.error(err);

let save = match({
   [PC_CLIENT_TYPE]: ({data, device}) => {
       data = parsePcRequest(data);
       device && (new Request({...data, type: PC_CLIENT_TYPE, device: device._id})).save()
   }
});

export function saveRequest(req, res) {
    let data = parsePcRequest(req.body)
    req.device.then((device, err) => err ? logError(err) : save({device, type: req.params.type, data: req.body}));
    res.json({ok: true});
}
