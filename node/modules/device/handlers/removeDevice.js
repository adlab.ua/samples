/**
 * Created by adlab on 21.01.16.
 */
export function removeDevice(req, res, next) {
    Promise.all([
        req.user,
        req.device
    ]).then(results => {
        let user = results[0],
            device = results[1];
        Promise.all([
            User.update({_id: user._id}, {stats: {...user.stats, devices: user.stats.devices - 1}}),
            Device.update({_id: device._id}, {status: STATUS_REMOVED})
        ])
        .then(_ => res.json({ok: true, deviceId: device._id}))
        .catch(err => res.json({ok: false, deviceId: device._id, error: err}))
    })
}