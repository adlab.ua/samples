/**
 * Created by adlab on 21.01.16.
 */
import Mongoose from 'mongoose'

export const DeviceSchema = Mongoose.Schema({
    alias: {type: String},
    type: {type: String},
    owner: {type: Mongoose.Schema.Types.ObjectId, ref: 'User'}
});

export const Device = Mongoose.model('Device', DeviceSchema);