/**
 * Created by adlab on 21.01.16.
 */
import {Device} from './../documents/Device'
export function injectDevice(req, res, next) {
    const deviceId = req.header('client') || req.query.client || req.body.client;
    req.device = Device.findOne({alias: deviceId}).populate('owner').exec()
    next()
}