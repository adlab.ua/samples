/**
 * Created by adlab on 20.01.16.
 */
import express from 'express'
import path from 'path'
import config from './../config/index'
import favicon from 'serve-favicon'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import passport from 'passport'
import LocalStrategy from 'passport-local'
import {User} from './user/documents/User'

import {router as CoreModule} from './core/index'

const app = express();
mongoose.connect(config.db.connection)

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(CoreModule);
app.listen(3000);