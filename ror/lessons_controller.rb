class Admin::Api::LessonsController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def index
    render :json => @admin_service.get_lessons_grouped
  end

  def create
    @lesson = Lesson.new lesson_params
    if @lesson.valid?
      @lesson.teacher = Group.find(params[:lesson][:group_id]).teacher
      l = Lesson.arel_table
      @event_exists = Lesson
      .where(
          l[:group_id].eq(@lesson.group_id).
              or(l[:teacher_id].eq(@lesson.teacher_id))
      )
      .where(
          (:startTime..:endTime).overlaps?(@lesson.startTime..@lesson.endTime)
      )
      .where(:day => @lesson.day)
      .first
      if @event_exists
        return render :json => {
            :success => false,
            :error => 'Lesson for this group already exists or time overlaps!'
        }
      else
        @lesson.save
        render :json => {
            :success => true
        }
      end
    else
      render :json => {
          :success => true,
          :error => 'Validation error!'
      }
    end
  end

  private
  def lesson_params
    params.require(:lesson).permit(:group_id, :day, :startTime, :endTime)
  end
end
