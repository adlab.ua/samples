/** @flow */
import React from 'react'
// import {pushPath} from
// import {listen, observeChange} from
import UserStore from './../../common/user/UserStore'
import {dispatch} from './../../common/application/RouterStore'
import {setToken} from '../resources/ApiRequest.jsx'


export function checkAuth(check:Function, failureRedirect:String="/"):Function {
    return (Component) => {
        @observeChange(['authenticated'])
        @listen(UserStore, ['authenticated'])
        class SecureComponent extends Component {
            componentDidMount() {
                super.componentDidMount && super.componentDidMount()
                Promise.resolve(check(this.state.authenticated)).then((result) => {
                    if(result) {
                        let token = UserStore.getState().token;
                        setToken(token)
                    } else {
                        dispatch(pushPath(failureRedirect))
                    }
                })
            }
        }

        return SecureComponent
    }
}
